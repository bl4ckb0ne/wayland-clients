# wayland-clients

Collection of [Wayland] clients for testing compositor and understanding the
protocols.

## Building

Install dependencies:

* meson
* wayland
* wayland-protocols

Run these commands to build:

    meson build/
    ninja -C build/

Install to the system with the following command:

    ninja -C build/ install

## Contributing

See [CONTRIBUTING.md]

[Wayland]: https://wayland.freedesktop.org/
[CONTRIBUTING.md]: https://gitlab.freedesktop.org/bl4ckb0ne/wayland-clients/-/blob/main/CONTRIBUTING.md
