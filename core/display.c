#include <stdio.h>
#include <wayland-client.h>

int main(int argc, char **argv) {
	/**
	 * The wl_display object represents a connection to the compositor. It
	 * handles all the data sent by the compositor.
	 * The client can connect to specific display using the WAYLAND_DISPLAY
	 * env var.
	 *
	 * See https://wayland-book.com/wayland-display/creation.html
	 */
	struct wl_display *display = wl_display_connect(NULL);
	if (display == NULL) {
		fprintf(stderr, "failed to connect to a wayland display\n");
		return 1;
	}

	/**
	 * Once the client is done with the wl_display, it can close the connection
	 * to the compositor and free all the resources associated with it.
	 * Events will no longer be processed passed this call.
	 */
	wl_display_disconnect(display);

	return 0;
}
