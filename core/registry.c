#include <stdio.h>
#include <wayland-client.h>

static void registry_global(void *data, struct wl_registry *registry,
		uint32_t name, const char *interface, uint32_t version) {
	/**
	 * This callback is executed when the compositor announces a new
	 * global interface to the client. The client may bind itself to it to
	 * start recieve events in the next dispatch call.
	 */
	printf("global '%"PRIu32"', version: %"PRIu32", interface: %s\n",
		name, version, interface);
}

static void registry_global_remove(void *data, struct wl_registry *registry,
		uint32_t name) {
	/**
	 * This callback is executed when the compositor announces the removal
	 * of a global interface. No more events will be send and no more
	 * request will be processed. If the global was bound by the client,
	 * all created objects should be destroyed.
	 */
	printf("global %"PRIu32" removed\n", name);
}

static const struct wl_registry_listener registry_listener = {
	.global = registry_global,
	.global_remove = registry_global_remove,
};

int main(int argc, char **argv) {
	struct wl_display *display = wl_display_connect(NULL);
	if (display == NULL) {
		fprintf(stderr, "failed to connect to a wayland display\n");
		return 1;
	}

	/**
	 * The wl_registry enumerates the globals advertised by the compositor.
	 * Globals will be sent to the client upon the creation of its
	 * wl_display. The compositor will send one event for each global to
	 * the client, and the client decides which global should be bound to
	 * suit its needs.

	 * See https://wayland-book.com/registry.html
	 */
	struct wl_registry *registry = wl_display_get_registry(display);

	/**
	 * Globals come and go as a result of changes from the compositor. The
	 * registry will send global and global_remove events to keep the
	 * client updated with the current state of the globals.
	 */
	wl_registry_add_listener(registry, &registry_listener, NULL);

	/**
	 * Once connected, the compositor will send events continuously to the
	 * client during its lifetime (until the wl_display is disconnected or the
	 * client is terminated).
	 * The `wl_display_dispatch` function will process the incoming events
	 * queued by the compositor.
	 *
	 * See https://wayland-book.com/wayland-display/event-loop.html
	 */
	if (wl_display_dispatch(display) == -1) {
		fprintf(stderr, "failed to dispatch events\n");
		return 1;
	}

	wl_display_disconnect(display);

	return 0;
}
